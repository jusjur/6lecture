package com.devbridge.sourcery.feedback.mapper;

import com.devbridge.sourcery.feedback.model.Question;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
@Mapper
public interface QuestionMapper {

  @Select("SELECT * FROM question WHERE id = #{id}")
  Question getQuestion(@Param("id") Integer id);

  @Delete("DELETE FROM question WHERE id = #{id}")
  Integer deleteQuestion(@Param("id") Integer question);

  @Update("UPDATE question SET " +
          "text = #{text}, " +
          "type = #{type}, " +
          "quiz_id = #{quiz_id}, " +
          "WHERE id = #{id}")
  Integer updateQuestion(Question question);

  @Select("INSERT INTO question (text, type, quiz_id) values (" +
          "#{text}, " +
          "#{type}, " +
          "#{quiz_id}) " )
  Question insertQuestion(Question question);
}
