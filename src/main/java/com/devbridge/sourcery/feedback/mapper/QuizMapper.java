package com.devbridge.sourcery.feedback.mapper;
import java.util.List;

import com.devbridge.sourcery.feedback.model.Answer;
import com.devbridge.sourcery.feedback.model.Question;
import com.devbridge.sourcery.feedback.model.Quiz;

import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;


@Repository
@Mapper
public interface QuizMapper {

  @Select("SELECT * FROM quiz WHERE code = #{code}")
  @Results(value = {
          @Result(property = "id", column = "id"),
          @Result(property = "questions", javaType=List.class, column="id",
                  many=@Many(select="getQuestions"))
  })
  Quiz findByCode(@Param("code") String code);

  @Select("SELECT * FROM question WHERE quiz_id = #{quiz_id}")
  @Results(value = {
          @Result(property = "id", column = "id"),
          @Result(property = "answers", javaType=List.class, column="id",
                  many=@Many(select="getAnswers"))
  })
  List<Question> getQuestions(Integer quiz_id);

  @Select("SELECT * FROM answer WHERE question_id = #{question_id}")
  List<Answer> getAnswers(Integer question_id);

  @Delete("DELETE FROM quiz WHERE code = #{code}")
  Integer deleteQuizByCode(@Param("code") String code);

  @Update("UPDATE quiz SET " +
          "title = #{title}, " +
          "description = #{description}, " +
          "active = #{active}, " +
          "code = #{code} " +
          "WHERE id = #{id}")
  Integer updateQuiz(Quiz quiz);

  @Select("INSERT INTO quiz (code, title, description, active) values (" +
          "#{code}, " +
          "#{title}, " +
          "#{description}, " +
          "#{active}) " )
  Quiz insertQuiz(Quiz quiz);
}
