package com.devbridge.sourcery.feedback.service;

import com.devbridge.sourcery.feedback.mapper.QuizMapper;
import com.devbridge.sourcery.feedback.model.Quiz;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class QuizService {

  private QuizMapper quizMapper;

  @Autowired
  public QuizService(QuizMapper quizMapper) {
    this.quizMapper = quizMapper;
  }

  public Quiz findByCode(String code) {
    return quizMapper.findByCode(code);
  }

  @Transactional
  public Integer deleteQuizByCode(String code) {
    return quizMapper.deleteQuizByCode(code);
  }

  @Transactional
  public Integer updateQuiz(Quiz quiz) {
    return quizMapper.updateQuiz(quiz);
  }

  @Transactional
  public Quiz insertQuiz(Quiz quiz) {
    return quizMapper.insertQuiz(quiz);
  }
}
