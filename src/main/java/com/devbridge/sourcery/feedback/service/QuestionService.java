package com.devbridge.sourcery.feedback.service;

import com.devbridge.sourcery.feedback.mapper.QuestionMapper;
import com.devbridge.sourcery.feedback.model.Question;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class QuestionService {

  private QuestionMapper questionMapper;

  @Autowired
  public QuestionService(QuestionMapper questionMapper) {
    this.questionMapper = questionMapper;
  }

  public Question getQuestion(Integer id) {
    return questionMapper.getQuestion(id);
  }

  @Transactional
  public Integer deleteQuestion(Integer id) {
    return questionMapper.deleteQuestion(id);
  }

  @Transactional
  public Integer updateQuestion(Question question) {
    return questionMapper.updateQuestion(question);
  }

  @Transactional
  public Question insertQuestion(Question question) {
    return questionMapper.insertQuestion(question);
  }

}
