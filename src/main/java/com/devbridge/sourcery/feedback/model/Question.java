package com.devbridge.sourcery.feedback.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Question {

    private Integer id;

    private String text;

    private QuestionType type;

    private List<Answer> answers;

}
