package com.devbridge.sourcery.feedback.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Feedback {

    private Integer id;

    private String quizId;

    private Map<String, Object> answers;

}

