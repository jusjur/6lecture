package com.devbridge.sourcery.feedback.api;

import com.devbridge.sourcery.feedback.model.Quiz;
import com.devbridge.sourcery.feedback.service.QuizService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@RestController
@RequestMapping(value = "/api/quiz")
public class QuizController {

  Logger logger = LoggerFactory.getLogger(QuizController.class);

  @Autowired
  private QuizService quizService;

  @PostMapping
  public ResponseEntity<Quiz> createQuiz(@RequestBody Quiz quiz) {
    logger.debug("createQuiz: {}", quiz);

    Quiz createdQuiz = quizService.insertQuiz(quiz);

    return new ResponseEntity<>(createdQuiz, HttpStatus.OK);
  }

  @PutMapping
  public ResponseEntity<Quiz> updateQuiz(@RequestBody Quiz quiz) {
    logger.debug("updateQuiz: {}", quiz);
    Integer rowsAffected = quizService.updateQuiz(quiz);
    logger.debug("Rows affected: " + rowsAffected);

    if (rowsAffected == 0) {
      return new ResponseEntity<>(HttpStatus.OK);
    } else {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
  }

  @GetMapping("/{quizCode}")
  public ResponseEntity<Quiz> getQuizByCode(@PathVariable String quizCode) {

    logger.debug("getQuizByCode: {}", quizCode);
    Quiz quiz = quizService.findByCode(quizCode);
    logger.debug("Quiz found: {}", quiz);

    if (quiz != null) {
      return new ResponseEntity<>(quiz, HttpStatus.OK);
    } else {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
  }

  @DeleteMapping("/{quizCode}")
  public ResponseEntity<Quiz> deleteQuizByCode(@PathVariable String quizCode) {

    logger.debug("deleteQuizByCode: {}", quizCode);
    Integer rowsAffected = quizService.deleteQuizByCode(quizCode);
    logger.debug("Rows affected: " + rowsAffected);

    if (rowsAffected == 0) {
      return new ResponseEntity<>(HttpStatus.OK);
    } else {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
  }

  @ExceptionHandler()
  public void handleException(){

  }

}
