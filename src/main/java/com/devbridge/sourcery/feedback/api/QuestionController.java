package com.devbridge.sourcery.feedback.api;

import com.devbridge.sourcery.feedback.model.Question;
import com.devbridge.sourcery.feedback.service.QuestionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/api/question")
public class QuestionController {

  Logger logger = LoggerFactory.getLogger(QuizController.class);

  @Autowired
  private QuestionService questionService;

  @GetMapping("/{id}")
  public ResponseEntity<Question> getQuestion(@PathVariable Integer id) {

    logger.debug("getQuestion: {}", id);
    Question question = questionService.getQuestion(id);
    logger.debug("Question found: {}", question);

    if (question != null) {
      return new ResponseEntity<>(question, HttpStatus.OK);
    } else {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
  }
}
