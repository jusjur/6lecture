insert into quiz ("code", "title", "description", "active") values ('LECTURE6', 'First Quiz', 'Test description', true);

insert into question ("quiz_id", "text", "type") values (1, 'Question 1', 'CHECK');
insert into question ("quiz_id", "text", "type") values (1, 'Question 2', 'RADIO');
insert into question ("quiz_id", "text", "type") values (1, 'Question 3', 'SHORT');

insert into answer (id, text, question_id) values (1, 'true', 1);